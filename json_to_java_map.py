import json

JSON_FILE = 'data.json'
JAVA_MAP_FILE = 'data.txt'
JAVA_MAP_NAME = 'map'

with open(JSON_FILE) as f:
    d = json.load(f)

with open(JAVA_MAP_FILE, 'a') as out:
    out.write('Map<String, String> map = new HashMap();\n')
    for attribute, value in d.items():
        out.write(f'map.put("{attribute}", "{value}");\n')

# todo: handle java string special characters like ", \ ...
